class Voc {
	private var vLetras = ('a' to 'z')++('A' to 'Z')
	private var vDigitos = (0 to 9)

	def ehLetra(x: Char) = vLetras.contains(x)
	def ehDigito(x: Int) = vDigitos.contains(x)
	
}


object vocabulario {
	def main() {
		val voc = new Voc
		var msg = "3 + 33 * (54 / 21)/abc"
		var msgArr = msg.toCharArray()
		for (x <- msgArr) {
			if (voc.ehLetra(x)) println("letra")
			if (voc.ehDigito(x)) println("digito")
			else println("nao eh digito nem numero")
		}
	}
}
