class Voc2 {
	// variaveis
	var vOperacoes = List('+', '-', '*', '\\')
	var vDigitos = ('0' to '9')
	var vLetras = ('a' to 'z')++('A' to 'Z')
	
	// metodos
	def ehOp(x: Char): Boolean = vOperacoes.contains(x)
	def ehDigito(x: Char): Boolean = vDigitos.contains(x)
	def ehLetra(x: Char): Boolean = vLetras.contains(x)
	def ehParentese(x: Char): Boolean = x match {
		case '(' => true
		case ')' => true
		case _ => false
	}
}

object Filtro {
	def main(args: Array[String]) {
		var msg = Console.readLine("> ")	
		avalia(msg)
	}

	def leTokens(s: String): List[String] = {
		var listaChar = s.toCharArray // transforma o parametro em uma lista de caracteres
		var token: String = ""        // inicia um token vazio

		for (i <- 0 to listaChar.length - 1) {

		}
	}

	def avalia(s: String): Unit = {
		var arr = s.toCharArray
		var v = new Voc2
		for (x <- arr) {
			if (v.ehLetra(x)) println("letra: " + x)
			else if (v.ehDigito(x)) println("digito: " + x)
			else println("nada " + x)
		}
	}
}
