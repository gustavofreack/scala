object Teste {
	def main(args: Array[String]) {
		val expr = "22 + 31 * abc32  - (32)"

		val listaTokens = tokenizer(expr)

		print("Tokens: ")
		listaTokens.foreach(x => print(x + ", "))
	}


	// Metodos que verificam se o parametro é letra, digito, operação ou parentese
	def ehLetra(c: Char): Boolean = (('a' to 'z')++('A' to 'Z')).contains(c)
	def ehDigito(c: Char): Boolean = ('0' to '9').contains(c)
	def ehOperacao(c: Char): Boolean = List('+', '-', '*').contains(c)
	def ehParentese(c: Char): Boolean = List('(', ')').contains(c)

	// metodo que monta um token recursivamente
	

	def montaNumeroInteiro(el :Array[Char]): String = {
		if (ehDigito(el.head) & !el.tail.isEmpty) return el.head + montaNumeroInteiro(el.tail)
		else if (ehDigito(el.head) & el.tail.isEmpty) return el.head.toString
		else return "" // retorna nada e para a recursão se houver digito
	}

	def montaNumero(el: Array[Char]): String = {
		if (ehDigito(el.head) & !el.tail.isEmpty) return el.head + montaNumero(el.tail)
		else if (el.head == '.' & !el.tail.isEmpty) return el.head + montaNumeroInteiro(el.tail)
		else return ""
	}


	// metodo que monta uma lista de tokens e retorna essa lista

	def tokenizer(expr: String): List[String] = {
		// abordagem iterativa
		var t = expr.toCharArray
		var token: String = ""
		var listaToken: List[String] = List()
		var i: Int = 0

		while (i < t.length) {
			if (ehLetra(t(i))) {
				while (ehLetra(t(i)) || ehDigito(t(i))) {
					token += t(i)
					i = i + 1
				}

				listaToken = listaToken :+ token
				token = ""
			}
			else if (ehDigito(t(i))) {
				do {
					token += t(i)
					i = i + 1
				} while (ehDigito(t(i)))

				listaToken = listaToken :+ token
				token = ""
			}
			else if (ehOperacao(t(i))) {
				token += t(i)
				listaToken = listaToken :+ token
				token = ""
				i = i + 1
			}
			else if (ehParentese(t(i))) {
				token += t(i)
				listaToken = listaToken :+ token
				token = ""
				i = i + 1
			}
			else if (t(i) == ' ') {
				// espaço, somente consumir ele
				i = i + 1
			}
			else {
				println("Erro de análise: " + t(i))
				i = t.length
			}
		}
		return listaToken
	}

	/*
	def tokenizer(t: Array[Char]): Unit = {
		t match {
			case head :: tail =>
				if (ehLetra(head)) {
					@tailrec
					def montaPalavra(el: Array[Char]): String = {
						if (ehLetra(el.head) & !el.tail.isEmpty) return el.head + montaPalavra(el.tail)
						else if (ehDigito(el.head) | ehLetra(el.head) & !el.tail.isEmpty) return el.head + montaPalavra(el.tail)
						else if (ehDigito(el.head) | ehLetra(el.head) & el.tail.isEmpty) return el.head
						else return ""
					}
					token = montaPalavra(tail)
				}
				else if (ehDigito(head)) {
					token = montaNumero(tail)
					/*
					token += head
					if (ehDigito(tail.head)) 
						token += tail.head + tokenizer(tail)
					else {
						listaToken = listaToken :+ token
						token = ""
					} */
				}
				else tokenizer(tail)
			case Nil => ""
		}
	}*/
} 
