class variaveis {
	def main() {
		var nome = "gustavo" // agora nome � uma variavel
		val nomeC = "santos" // agora nomeC � uma constante
		
		println(s"nome: $nome e nomeC = $nomeC")
	}
}
