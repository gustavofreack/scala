class Teste {
    private var nome: String = ""
    private var idade: Int = 0
    
    def setIdade = (x: Int) = idade = x
    def setNome = (x: String) = nome = x
    def getIdade = idade
    def getNome = nome
}

object Main extends App {
    val t = new Teste
    t.setNome("Gustavo")
    t.setIdade(20)
    
    printf("%s %d\n", t.getNome, t.getIdade)
}